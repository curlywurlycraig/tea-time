import React from 'react';
import SvgBody from '../SvgBody/SvgBody';
import PropTypes from 'prop-types';

class Box extends React.Component {
    static contextTypes = {
        loop: PropTypes.object
    };

    render() {
        return <SvgBody
            drawSvg={this.drawSvg.bind(this)}
            restitution={0.4}
            {...this.props}
            />
    }

    drawSvg(body) {
        return <svg width="50" height="50" style={this.getSvgStyle(body)} >
            <rect
                width="50"
                height="50"
                style={this.getRectStyle() } />

            <text x="50%" y="50%" fill="#aaa" style={this.getTextStyle()}>Tits</text>
        </svg>
    }

    getRectStyle() {
        return {
            fill: 'white',
            width: 50,
            height: 50
        }
    }

    getSvgStyle(body) {
        return {
            position: 'absolute',
            left: body.position.x - 25,
            top: body.position.y - 25,
            transform: this.getRotationString.bind(this)(body),
        }
    }

    getTextStyle() {
        return {
            y: '50%',
            x: '50%',
            alignmentBaseline: 'middle',
            textAnchor: 'middle',
            color: '#bbb'
        }
    }

    getRotationString(body) {
        return `rotate(${body.angle}rad)`;
    }
}

export default Box;
import React from 'react';
import { Body } from 'react-game-kit';
import PropTypes from 'prop-types';

class SvgBody extends React.Component {
    static contextTypes = {
        loop: PropTypes.object
    };

    static propTypes = {
        svg: PropTypes.node,
    }

    constructor() {
        super();

        this.state = {
            lastX: null,
            lastY: null
        }
    }

    update() {
        if (!this.body) {
            return;
        }

        if (Math.round(this.body.body.position.x) !== Math.round(this.state.lastX) ||
            Math.round(this.body.body.position.y) !== Math.round(this.state.lastY)) {
            this.forceUpdate();
            this.setState({
                lastX: this.body.body.position.x,
                lastY: this.body.body.position.y
            });
        }
    };

    setBody(body) {
        this.body = body;

        // this is where we'll set the body via Matter's Svg API.
    }

    render() {
        return <Body ref={this.setBody.bind(this)} {...this.props}>
            { this.body ? this.props.drawSvg(this.body.body) : null }
        </Body>
    }

    componentDidMount() {
        this.context.loop.subscribe(this.update.bind(this));
    }

    componentWillUnmount() {
        this.context.loop.unsubscribe(this.update.bind(this));
    }
}

export default SvgBody;
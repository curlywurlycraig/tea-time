import React, { Component } from 'react';
import { Loop, Stage, World, Body } from 'react-game-kit';

import Box from './components/Box/Box.js';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  shouldComponentUpdate() {
    return true;
  }

  /** By default, the stage tries to compute the aspect ratio in an odd way.
   * I'd prefer if it just used content reflow naturally. Fortunately you can override the stage styles
   * rather than letting it compute them.
   */
  getStageStyle() {
    return {
      width: '100%',
      height: '100%',
      position: 'relative',
      transform: 'none'
    };
  }

  render() {
    return (
      <Loop>
        <div className="GameContainer">
          <Stage style={this.getStageStyle()}>
            <World gravity={{
              x: 0,
              y: 1,
              scale: 0.001}}>

              <Box
                args={[350, 0, 50, 50]}
                angle={0.3} />

              <Box
                args={[300, -100, 50, 50]}
                angle={0} />

              <Body friction={1} frictionStatic={1} isStatic={true} args={[0, 700, 5000, 500]}>
                <div className="Floor" style={{position: 'absolute', top: 450, left: 0, width: '100%', height: 500}}></div>
              </Body>

            </World>
          </Stage>
        </div>
      </Loop>
    );
  }
}

export default App;
